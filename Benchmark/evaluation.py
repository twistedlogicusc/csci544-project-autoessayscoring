#Name: Di Yang
#Email: diy@usc.edu

#This is the evaluation file to get scores(qwk) for each of the 8 sets,
#and the final score(average qwk) for the whole.

#Command line:
#python3 evaluation.py HUMAN_RATES AUTO_RATES
#HUMAN_RATES and AUTO_RATES should follow 2-column format(separate by comma),
#i.e. SET_ID, PREDICTED_SCORE 

if __name__ == "__main__":
	import os
	import sys
	import score 

	humanScore = sys.argv[1]
	autoScore = sys.argv[2]
	with open(humanScore, "r") as finHuman:
		with open(autoScore, "r") as finAuto:
			vectorHuman = []
			vectorAuto = []
			for i in range(0,8):
				temp1 = []
				temp2 = []
				vectorHuman.append(temp1)
				vectorAuto.append(temp2)
			for line in finHuman:
				line = line.split(',')
				tag = line[0]
				result = line[1].replace('\n', '')
				if tag == '1':
					vectorHuman[0].append(int(result))
				if tag == '21' or tag == '22':
					vectorHuman[1].append(int(result))
				if tag == '3':
					vectorHuman[2].append(int(result))
				if tag == '4':
					vectorHuman[3].append(int(result))
				if tag == '5':
					vectorHuman[4].append(int(result))
				if tag == '6':
					vectorHuman[5].append(int(result))
				if tag == '7':
					vectorHuman[6].append(int(result))
				if tag == '8':
					vectorHuman[7].append(int(result))

			for line in finAuto:
				line = line.split(',')
				tag = line[0]
				result = line[1].replace('\n', '')
				if tag == '1':
					vectorAuto[0].append(int(result))
				if tag == '21' or tag == '22':
					vectorAuto[1].append(int(result))
				if tag == '3':
					vectorAuto[2].append(int(result))
				if tag == '4':
					vectorAuto[3].append(int(result))
				if tag == '5':
					vectorAuto[4].append(int(result))
				if tag == '6':
					vectorAuto[5].append(int(result))
				if tag == '7':
					vectorAuto[6].append(int(result))
				if tag == '8':
					vectorAuto[7].append(int(result))
			qwk = []
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[0],vectorAuto[0],2,12))
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[1],vectorAuto[1],1,6))
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[2],vectorAuto[2],0,3))
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[3],vectorAuto[3],0,3))
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[4],vectorAuto[4],0,4))
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[5],vectorAuto[5],0,4))
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[6],vectorAuto[6],0,30))
			qwk.append(score.quadratic_weighted_kappa(vectorHuman[7],vectorAuto[7],0,60))
			for i in range(0,8):
				print("qwk_{0}: ".format(i+1) + str(qwk[i]))
			print("mean_quadratic_weighted_kappa: " + str(score.mean_quadratic_weighted_kappa(qwk)))