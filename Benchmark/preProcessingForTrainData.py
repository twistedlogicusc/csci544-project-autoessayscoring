
import sys
import codecs

def rawDataPreProcess():
	inputFilePath = sys.argv[1];
	outputFilePath = sys.argv[2];
	#inputFilePath = "training_set_rel3.tsv";
	#outputFilePath = "testOut.tsv";
	fpRead = codecs.open(inputFilePath,'r',encoding='cp1252', errors='ignore');
	fpWrite = open(outputFilePath, 'w+');
	startWrite1 = False;
	lineNum = 1;
	line = fpRead.readline(); # skip the header of the raw data
	newLine = "";
	newLine += "essayID" + "\t" + "setID" + "\t" + "contents" + "\t" + "scoreLabel"
	fpWrite.write(newLine+"\n"); # print out the header of pre-processed data
	for line in fpRead:
		print("Processed " + str(lineNum) + " lines. ");
		line = line.rstrip('\n');
		splittedLine = line.split('\t');
		setID = splittedLine[1];
		newLine = "";
		startWrite2 = False;
		for i in range(0, len(splittedLine)):
			tempItem = "";
			if (setID=="1"):
				if (i<3 or i==6):
					if(i==2):
						if( splittedLine[i][0]=='"' and splittedLine[i][-1]=='"' ):
							tempItem = splittedLine[i][1:-1];
						else:
							tempItem = splittedLine[i];
					else:
						tempItem = splittedLine[i];
				elif i>6:
					break;
			if (setID=="2"):
				if i==6:
					if( splittedLine[2][0]=='"' and splittedLine[2][-1]=='"' ):
						newLine += splittedLine[0] + "\t" + splittedLine[1]+"1" + "\t" + splittedLine[2][1:-1] + "\t" + splittedLine[i] + "\n";						
					else:
						newLine += splittedLine[0] + "\t" + splittedLine[1]+"1" + "\t" + splittedLine[2] + "\t" + splittedLine[i] + "\n";
				elif i==9:
					if( splittedLine[2][0]=='"' and splittedLine[2][-1]=='"' ):
						newLine += splittedLine[0] + "\t" + splittedLine[1]+"2" + "\t" + splittedLine[2][1:-1] + "\t" + splittedLine[i];						
					else:
						newLine += splittedLine[0] + "\t" + splittedLine[1]+"2" + "\t" + splittedLine[2] + "\t" + splittedLine[i];	
				elif i>9:
					break;
			if (setID=="3" or setID=="4" or setID=="5" or setID=="6" or setID=="7" or setID=="8"):
				if (i<3 or i==6):
					if(i==2):
						if( splittedLine[i][0]=='"' and splittedLine[i][-1]=='"' ):
							tempItem = splittedLine[i][1:-1];
						else:
							tempItem = splittedLine[i];
					else:
						tempItem = splittedLine[i];
					#tempItem = splittedLine[i];
				elif i>6:
					break;
			if startWrite2==False and len(tempItem)!=0:
				newLine += tempItem;
				startWrite2 = True;
			elif len(tempItem)!=0:
				newLine += "\t" + tempItem;	
		if startWrite1==False:
			fpWrite.write(newLine);
			startWrite1 = True;
		else:
			fpWrite.write("\n" + newLine);
		lineNum += 1;
	
	fpRead.close();
	fpWrite.close();
	
	
if __name__ == "__main__":
	rawDataPreProcess();


