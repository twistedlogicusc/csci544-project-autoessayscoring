#!/usr/bin/perl

open(FILE, "<", "trainMat")
	or die "cannot open < trainAll: $!";
#-----------------------------------------------------#
#  read the entire file into the array named 'lines'  #
#-----------------------------------------------------#

chomp (@lines = (<FILE>));
close(FILE);

open(FILE, "<", "traincombin")
or die "cannot open < traincombin: $!";
chomp(@ids = (<FILE>));
close(FILE);
#train.feature.v2

open(FILE, "<", "train.feature.v2")
or die "cannot open < train.feature.v2: $!";
chomp(@others = (<FILE>));
close(FILE);
open(FILE, ">", "combinTrain")
or die "cannot open < combinTrain: $!";



$i = 0;
print @lines.length," ",@ids.length,"\n";

#-----------------------------------------------------#
#  for trainning data we build a hashmap to store it  #
#-----------------------------------------------------#


%trainId = {};

for($lineNum = 0 ; $lineNum<@lines.length;$lineNum++){
    $idInfo = $ids[$lineNum];
    $feature = substr($lines[$lineNum],0,-1);
    @idinfos = split(/\s+/ , $idInfo);
    $essayId = $idinfos[0];
    $setId = $idinfos[1];
    $label = $idinfos[2];
    #print $essayId,'-',$setId,'-',$label,"\n";
    #last;
    $key =$essayId."-".$setId."-".$label;
    #print $key,"\n";
    #last;
    $trainId{$key} = $feature;
    #print $feature;
    #last;
}


#-----------------------------------------------------#
#  rearrange the result with original sequence        #
#   and cascade other features with it                #
#-----------------------------------------------------#
$match = 0;
$mismatch = 0;
print FILE "essayId",",","setId",",","label",",","totalFeature","\n";
for($lineNum = 1; $lineNum<@others.length; $lineNum++){
    
    $line = $others[$lineNum];
    @elements = split(/,/,$line);
    $essayId = $elements[0];
    $setId = $elements[1];
    $label = $elements[2];
    $key = $essayId."-".$setId."-".$label;
    if(exists( $trainId{$key} ) ){
        $match++;
        #buildup features
        $pca = $trainId{$key};
        @pcaEle = split(/,/ , $pca);
        #print $key,":\n";
        $totalFeature =  substr($pca.",".join(",",@elements[3..@elements.length]),0,-1);
        #print $totalFeature;
        print FILE $essayId,",",$setId,",",$label,",",$totalFeature,"\n";
        #last;
    }
    else{
        $mismatch++;
    }
    
}

close(FILE);
#-----------------------------------------------------#
#  for test data we build a hashmap to store it       #
#-----------------------------------------------------#




#-----------------------------------------------------#
#  for test data we build a hashmap to store it       #
#-----------------------------------------------------#