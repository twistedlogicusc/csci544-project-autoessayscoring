#!/usr/bin/perl

open(FILE, "<", "trainAll")
	or die "cannot open < trainAll: $!";
#-----------------------------------------------------#
#  read the entire file into the array named 'lines'  #
#-----------------------------------------------------#

chomp (@lines = (<FILE>));
close(FILE);
$i = 0;
foreach $line (@lines){
    $i++;
    @elements = split(/\t/,$line);
    $essayId = $elements[0];
    $setId = $elements[1];
    $content = $elements[2];
    $label = $elements[-1];
    $fileName = "./train/".$label."/".$essayId.$setId;
    open(FILE2, ">", $fileName)
    or die "cannot open < $fileName: $!";
    print "$i | writing $content to fileName : $fileName\n";
    print FILE2 $essayId," ",$setId," ",$content;
}




