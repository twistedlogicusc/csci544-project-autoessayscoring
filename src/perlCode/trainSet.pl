#!/usr/bin/perl

open(FILE, "<", "perlCode.arff")
	or die "cannot open < train_feature.arff: $!";
open(seqFile, "<", "../text_example.arff")
or die "cannot open < text_example.arff: $!";
open(OUT, ">", "label.fea")
  or die "cannot open < label.fea: $!";
open(OUT2, ">", "check")
  or die "cannot open < check: $!";
open(OUT3, ">", "trainId")
or die "cannot open < trainId: $!";
#-----------------------------------------------------#
#  read the entire file into the array named 'lines'  #
#-----------------------------------------------------#

chomp (@lines = (<FILE>));
close(FILE);
chomp (@seqLine = (<seqFile>));
#--------------------------------------------------------#
#  print each line, with the line number printed before  #
#  the actual line                                       #
#--------------------------------------------------------#

$i = 1;
@attri = [];
$attri[0] = "#";
@values = [];
%indexMap = {};
#5721 attributes
#create maps and arrays
foreach $line (@lines) {
   #print "$i|  $line\n";
   #$i++;
   	if($line =~ /numeric$/){
        @elements = split(/\s+/,$line);
        $attri[$i] = $elements[1];
        $indexMap{$elements[1]} = $i;
   		#print $i," | ",$elements[1],"\n","\n";
        $i++;
	}
}
#--------------------------------------------------------#
#  create correspondence output with class and attribute #
#  the actual line                                       #
#--------------------------------------------------------#
$i = 1;
$zeroNum = 0;
$otherNum = 0;
foreach $line (@lines) {
   #print "$i|  $line\n";
   #$i++;
   	if($line =~ /^{/ && $line=~/}$/){
        #print $line,"\n\n\n";
        @elements = split(/[,{}]/,$line);
        
        if($elements[1] =~ /^0/){
          $zeroNum++;
          @labelPair = split(/\s/,$elements[1]);
          #print labels
          print OUT $labelPair[1]," ";
          #print features
          $feaInd = 2;
          for(;$feaInd<@elements.length;$feaInd++){
            @indexPair = split(/\s/,$elements[$feaInd]);
            if(@indexPair.length == 2){
              print OUT $indexPair[0],":",$indexPair[1]," ";
              print OUT2 $attri[$indexPair[0]]," ";
              #update values
              $values[$indexPair[0]] = $indexPair[1];
            } 
          }
          #end of the line
          print OUT "\n";
          print OUT2 "\n";
        }
        else{
          $otherNum++;
          print OUT "0 ";
          #print features
          $feaInd = 0;
          for(;$feaInd<@elements.length;$feaInd++){
            @indexPair = split(/\s/,$elements[$feaInd]);
            if(@indexPair.length == 2){
              print OUT $indexPair[0],":",$indexPair[1]," ";
              print OUT2 $attri[$indexPair[0]]," ";
              #update values
              $values[$indexPair[0]] = $indexPair[1];
            } 
          }
          #end of the line
          print OUT "\n";
          print OUT2 "\n";
          
        }
   		
        $i++;
	}

}
foreach $seqes (@seqLine){
    
    if($seqes =~ /^\'/){
        #print $seqes,"\n";
        @allSeq = split( / /,$seqes);
        my $essayId = $allSeq[0];
        my $essayIdNum = substr($essayId,1);
        my $setId = $allSeq[1];
        print OUT3 $essayIdNum," ",$setId,"\n";
    }
    
}
close(OUT);
close(OUT2);
close(out3);
print $zeroNum, "  ",$otherNum,"\n";
#-----------------------------------------------------#
#build up test cases
#-----------------------------------------------------#

open(FILE, "<", "../testAll")
  or die "cannot open < testAll: $!";
open(OUT, ">", "test.fea")
  or die "cannot open < test.fea: $!";
open(OUT2, ">", "check2")
  or die "cannot open < check2: $!";
open(OUT3, ">", "testIdSeq")
  or die "cannot open < testIdSeq: $!";
#-----------------------------------------------------#
#  read the entire file into the array named 'lines'  #
#-----------------------------------------------------#

chomp (@lines = (<FILE>));
close(FILE);


foreach $line (@lines){
    my %lineArr = {};
    #split test cases into tokens
    
    @elements = split(/\t/ , $line);
    
    $eassyId = $elements[0];
    $setId = $elements[1];
    $content = $elements[2];
    $label = $elements[-1];
    $lineArr{$indexMap{$setId}} = $indexMap{$setId};
    #split content with tokens
    #write a file with label and feature with \" \\r\\n\\t.,;:\\\'\\\"()?!\"'
    
    @tokens = split(/[\"\s\r\n\t.,;:\'\\"()?!\"]/ , $content);
    
    for($i = 0 ; $i<@tokens.length;$i++){
        if(exists($indexMap{ $tokens[$i] })){
            $lineArr{ $indexMap{ $tokens[$i] } }  = $indexMap{ $tokens[$i] };
            #print "insert $indexMap{ $tokens[$i] } from : $tokens[$i] \n";
        }
    }
    if(exists($indexMap{ $setId })){
            $lineArr{ $indexMap{ $setId } }  = $indexMap{ $setId };
            #print "insert $indexMap{ $tokens[$i] } from : $tokens[$i] \n";
    }
    my @indexs = [];
    $i = -1;
    #$indexs[0] = $indexMap{$setId} + 0 -1 + 1;
    foreach $key (keys(%lineArr)){
      $i++;
      $indexs[$i] = $lineArr{$key} + 0 -1 + 1;
    }
    @indexs = sort {$a <=> $b} @indexs;
=pod
    foreach $tmp (@indexs){
      print $tmp," - ";
    }
    print "\n";
    last;
    =cut
    #another file with ID with the same order
=cut
    #write the class num as well as the features

    print OUT $label," ";
    print OUT2 $label," ";
    foreach $section (@indexs){
      if($section != 0){
          print OUT $section,":",$values[$section]," ";
          print OUT2 $attri[$section]," ";
      }
    }
    print OUT "\n"; 
    print OUT2 "\n";
    print OUT3 $eassyId," ", $setId ,"\n";
}