open(FILE, "<", "combinTest")
or die "cannot open < combinTest: $!";

#------------------------------#
chomp (@lines = (<FILE>));
close(FILE);

open(FILE, ">", "testSvm")
or die "cannot open > testSvm: $!";
open(FILE2, ">", "testSetId")
or die "cannot open > testSvm: $!";
#for 1:n do something else

for($i=1; $i<@lines.length;$i++){
    $line = $lines[$i];
    @elements = split(/,/, $line);
    $label = $elements[2];
    $setId = $elements[1];
    $outLine = $label." ";
    for($j=3;$j<@elements.length;$j++){
        $tmp = ($j-2).":".$elements[$j]." ";
        $outLine .= $tmp;
    }
    print FILE $outLine,"\n";
    print FILE2 $setId,",\n";
}
close(FILE);
close(FILE2);

open(FILE2, ">", "trainSetId")
or die "cannot open > testSvm: $!";
open(FILE, "<", "combinTrain")
or die "cannot open < combinTrain: $!";

#------------------------------#
chomp (@lines = (<FILE>));
close(FILE);

open(FILE, ">", "trainSvm")
or die "cannot open > trainSvm: $!";

#for 1:n do something else

for($i=1; $i<@lines.length;$i++){
    $line = $lines[$i];
    @elements = split(/,/, $line);
    $label = $elements[2];
    $setId = $elements[1];
    $outLine = $label." ";
    for($j=3;$j<@elements.length;$j++){
        $tmp = ($j-2).":".$elements[$j]." ";
        $outLine .= $tmp;
    }
    print FILE $outLine,"\n";
    print FILE2 $setId,",\n";
}
close(FILE);
close(FILE2);