clc
clear all

load('libsvm.mat')

trainLabel = combinTrain(:,3);
trainData = combinTrain(:,4:37);

testLabel = combinTest(:,3);
testData = combinTest(:,4:37);

testId = combinTest(:,2);
optSet = [];
accuracySet = [];
predictSet = zeros(24,2937);
t=0;
c=0;
 opt = ' -s -v 5';
 op1 = strcat(' -t ',{' '},num2str(t));
 op2 = strcat(' -c ',{' '},num2str(c));
 opt = strcat(opt,op1,op2);

model_linear = lsvmtrain(trainLabel, trainData,opt);
optSet = [optSet,opt];
[predict_label_L, accuracy_L, dec_values_L] = lsvmpredict(testLabel, testData, model_linear);
accuracySet = [accuracySet,accuracy_L];
predictSet((t+1)*(c+1),:) = predict_label_L;