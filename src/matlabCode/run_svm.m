% the data is very unbalanced, may need to try regression, and linear svm
run configuration.m
run add_path.m

clear all; clc; close all;

load('../data/data.mat');
load('../data/test_set_id');
load('../data/train_set_id');

X = combinTrain;
y = train_label;


% normalization
X_mean = mean(X, 1);
X_std = nanstd(X);

for col = 1:size(X,2)
    if X_std(col) > 0
        X_norm(:, col) = (X(:, col) - repmat(X_mean(col), size(X, 1), 1))./repmat(X_std(col), size(X, 1), 1);
    else
        X_norm(:, col) = X(:, col) - repmat(X_mean(col), size(X, 1), 1);
    end
end

%% cross validation

C = 4.^(-4:5);
g = 10.^(-4:5);

nC = length(C);
nG = length(g);

all_predict_label = [];

uni_train_set_id = unique(train_set_id);

for i = 1:length(uni_train_set_id)
    train_set_index = find(train_set_id == uni_train_set_id(i));
    X_norm_sub = X_norm(train_set_index, :);
    y_sub = y(train_set_index, :);
    
    rng(1)
    CVO = cvpartition(y_sub, 'k', 5);

% TODO, feature selection
    for nfeature = 34
%      for nfeature = 2:12
        sel_index = feast('mim', nfeature , X_norm_sub, y_sub);
        X_sel = X_norm_sub(:, sort(sel_index));
        
        for c_index = 1:nC
            for g_index = 1:nG
                option = ['-q -s 0 -t 2 -c ', num2str(C(c_index)),' -g ', num2str(g(g_index))];
                % option = ['-q -s 3 -t 2 -c ', num2str(C(c_index)),' -g ', num2str(g(g_index))];
                
                for j = 1:CVO.NumTestSets
                    index_train = CVO.training(j);
                    index_test = CVO.test(j);
                    X_train = X_sel(find(index_train == 1), :);
                    X_test = X_sel(find(index_test == 1), :);
                    y_train = y(find(index_train == 1));
                    y_test = y(find(index_test == 1));
                    
                    model = svmtrain(y_train, X_train, option);
                    [y_predict, accu(:,j), prob_predict] = svmpredict(y_test, X_test, model, '-q');
                    
                end
                cv_accuracy_mean(c_index, g_index) = mean(accu(1, :));
                cv_accuracy_std(c_index, g_index) = std(accu(1, :));
            end
        end
        
        [max_cv_accu, index] = max(max(cv_accuracy_mean));
        [row, col] = find(cv_accuracy_mean == max_cv_accu, 1);
        C_best(nfeature) = C(row);
        g_best(nfeature) = g(col);
        
        accu_sel_mean(nfeature) = max_cv_accu;
        accu_sel_std(nfeature) = cv_accuracy_std(row, col);
    end
    
    [max_accu_final, best_dim] = max(accu_sel_mean);
    final_accu_std = accu_sel_std(best_dim);
    
    %% test
    sel_index = feast('mim', best_dim, X_norm_sub, y_sub);
    %   the order of feature  could not change, otherwise the performance will drop
    X_sel = X_norm_sub(:, sort(sel_index)); 
    
    option_best = ['-q -s 0 -t 2 -c ', num2str(C_best(best_dim)),' -g ', num2str(g_best(best_dim))];
    model = svmtrain(y_sub, X_sel, option_best);
    
    X_test = combinTest;
    y_test = test_label;

    for col = 1:size(X_test,2)
        if X_std(col) > 0
            X_test_norm(:, col) = (X_test(:, col) - repmat(X_mean(col), size(X_test, 1), 1))./repmat(X_std(col), size(X_test, 1), 1);
        else
            X_test_norm(:, col) = X_test(:, col) - repmat(X_mean(col), size(X_test, 1), 1);
        end
    end
    
    uni_test_set_id = unique(test_set_id);
    test_set_index = find(test_set_id == uni_test_set_id(i));
    
    X_test_norm_sub = X_test_norm(test_set_index, :);
    y_test_sub = y(test_set_index, :);
    
    [predict_label, test_accu, predict_accu] = svmpredict(y_test_sub, X_test_norm_sub, model);
    all_predict_label(test_set_index) = predict_label;
end

all_predict_label = all_predict_label';
%% Plot figure
% plot(accu_sel_mean(2:nfeature))
% max_accu_final
% best_dim
% final_accu_std


%% compute confusion matrix
confusion_matrix = confusionmat(all_predict_label,y_test)/length(y_test);
% imagesc(confusion_matrix)
% colorbar
% colormap(jet)
% export_fig('../figure/2class_confusion.pdf',gcf);


%% compute kappa
dlmwrite('../data/predict_label', all_predict_label);
system('paste -d, ../data/test_set_id ../data/test_label > HUMAN_RATES');
system('paste -d, ../data/test_set_id ../data/predict_label > AUTO_RATES');
system('python evaluation.py HUMAN_RATES AUTO_RATES > result');

