labels = zeros(1,2937);
features = zeros(2937,5731);
for i=1:2937
    i
   tmp = cellfun('length',C{i});
    
   numEle = length(tmp);
   labels(i) = str2num(C{i}{1});
   
   for j=2:(numEle-1)
       pair = strsplit(C{i}{j},':');
       
        id = str2num(pair{1});
        value = str2double(pair{2});
        features(i,id) = value;
       
   end
    
end