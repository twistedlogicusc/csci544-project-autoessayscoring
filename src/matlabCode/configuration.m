% format
format compact;

% figure
set(0,'defaultAxesXGrid', 'on');
set(0,'defaultAxesYGrid', 'on');
set(0,'defaultAxesZGrid', 'on');
set(0,'DefaultLineLineWidth', 1.5); 
set(0,'DefaultLineMarkersize', 10); 
set(0,'DefaultTextFontsize', 14); 
set(0,'DefaultAxesFontsize', 14); 
set(0,'DefaultAxesFontName', 'Helvetica');
set(0,'DefaultFigurePaperPositionMode', 'auto');
set(0,'defaultfigurecolor', 'w');