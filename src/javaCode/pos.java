package pos2;

import java.io.IOException;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import java.util.*; 
import java.io.*; 

public class pos {
	public static void main(String[] args) {
		MaxentTagger tagger = new MaxentTagger("english-bidirectional-distsim.tagger");
		String inputFileName = "testAll";
		String outputFileName = "testAll_tagged";
		File inputFile = new File(inputFileName);
		File outputFile = new File(outputFileName);

        BufferedReader reader = null;
        BufferedWriter writer = null;  
        try {
            reader = new BufferedReader(new FileReader(inputFile));
            writer = new BufferedWriter(new FileWriter(outputFile));
            String tempString = null;
            boolean isFirst = true;
            int lineNum = 1;
            while ((tempString = reader.readLine()) != null) {
//            	if (isFirst) {
//            		writer.write(tempString + "\n");
//            		isFirst = false;
//            		continue;
//            	}
            	String[] lineArr = tempString.split("\\t");
            	String taggedLine = tagger.tagString(lineArr[2]);
        		writer.write(lineArr[0] + "\t" + lineArr[1] + "\t" + taggedLine + "\t" + lineArr[3] + "\n");
                System.out.println(lineNum++);
                //break;
            }
            writer.flush();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
       // The tagged string
       // String tagged = tagger.tagString(sample);
 
        // Output the result
        //System.out.println(tagged);
	}
}
