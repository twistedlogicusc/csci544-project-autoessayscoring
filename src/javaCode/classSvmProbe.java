import libsvm.*;
import java.io.*;
import java.util.*;

class classSvmProbe{
    public static void main(String[] args) throws IOException {
        svm_train svmt = new svm_train();
        
        svm_predict svmp = new svm_predict();
        
        String[] argvTrain = {
            "-v","10","-t","2","-g","1","-c","5","-d","5",

            "./trainSvm",// 训练文件
            
            "./classModel"// 模型文件
            
        };
        
        String[] argvPredict = {
            "./testSvm",// 预测文件
            
            "./classModel", // 模型文件
            
            "classResult" // 预测结果文件
            
        };
        
        try {
            
            //svmt.main(argvTrain);
            
            svmp.main(argvPredict);
            
        } catch (IOException e) {
            
            e.printStackTrace();
            
        }
        
        
    }
}