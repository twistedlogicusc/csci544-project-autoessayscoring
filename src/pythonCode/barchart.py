
import plotly.plotly as py
from plotly.graph_objs import *


d = [
     0.673635255,
     0.677454775,
     0.664120793,
     0.583879231,
     0.69222351,
     0.664091194,
     0.128134849,
     0.223275381,
     0.24294543,
     0.302186428,
     0.466102113,
     0.603880098,
     0.556570096,
     0.633712135,
     0.70879359
]
n=[
   'distinct wordcount',
   'character count',
   'word count',
   'sentence count',
   'Count product',
   '3 gram sum',
   '3 gram sqr dev',
   '3 gram min',
   '3 gram max',
   '3 gram range',
   'postag RB',
   'postag VB',
   'postag JJ',
   'postag NN',
   'tf-idf after PCA'

]

has = {}

for i in  range(0,len(n)):
  has[d[i]] = n[i];

dsort = sorted(d);

newd = [];
newn = [];

for ele in dsort:
  newd.append(ele)
  newn.append(has[ele])

data = Data([
    Bar(
        x=newn,
        y=newd,
        marker=Marker(
            color='rgb(142, 124, 195)'
        )
    )
])
layout = Layout(
    title='Single Feature Kappa Value',
    font=Font(
        family='Raleway, sans-serif'
    ),
    showlegend=False,
    xaxis=XAxis(
        tickangle=-45
    ),
    yaxis=YAxis(
        zeroline=False,
        gridwidth=2
    ),
    bargap=0.05
)
fig = Figure(data=data, layout=layout)
plot_url = py.plot(fig, filename='bar-with-hover-text')