#Name: Di Yang
#Email: diy@usc.edu
#python3 preprocess_project.py 
stopWordsDict = {};
def removeStopWords(essay):
    global stopWordsDict;
    tempEssay = essay.rstrip("\n");
    splittedTempEssay = tempEssay.split(" ");
    newEssay = "";
    for index in range(0,len(splittedTempEssay)):
        originWord = splittedTempEssay[index];
        tempWord = originWord.lower();
        searchObj = re.search( r'([@]\w{1,})|(\w{1,})', tempWord);
        if searchObj is None:
            newEssay += originWord + " ";
        else:
            tempWord = searchObj.group(0);
            if tempWord in stopWordsDict:
                continue;
            else:
                newEssay += originWord + " ";
    newEssay = newEssay[0:len(newEssay)-1];
    return newEssay;

if __name__ == "__main__":
	import os
	import sys
	import nltk
	import re


	global stopWordsDict;
	fpOpen = open("stopwords.txt",'r',errors='ignore');
	for line in fpOpen:
		line = line.rstrip("\n");
		if line in stopWordsDict:
			continue;
		else:
			stopWordsDict[line] = 1;
	fpOpen.close();

	srcFile = sys.argv[1]
	fileB = sys.argv[2]
	with open(fileB, "w") as foutB:
		with open(srcFile, "r", errors = 'ignore') as fin:
			text = fin.read()
			sentenceSplit = nltk.tokenize.sent_tokenize(text)
			for s in sentenceSplit:
				#s = removeStopWords(s);
				foutB.write(s + '\n')



			