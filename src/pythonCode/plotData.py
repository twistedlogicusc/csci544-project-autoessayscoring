import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import math
testFile = open('combinTest')
trainFile = open('combinTrain')

lines = {}
testNum = 0;
trainNum = 0;


while True:
  line = testFile.readline()
  if len(line) == 0:
    break;
  line = line.rstrip('\n')
  eles = line.split(',')
  label = int(eles[2])
  labelGroup = math.ceil(label/4);
  if labelGroup in lines.keys():
    lines[labelGroup][0] +=1;
  else:
    lines[labelGroup] = [0,0]
  testNum+=1


while True:
  line = trainFile.readline()
  if len(line) == 0:
    break;
  line = line.rstrip('\n')
  eles = line.split(',')
  label = int(eles[2])
  labelGroup = math.ceil(label/4);
  if labelGroup in lines.keys():
    lines[labelGroup][1] +=1;
  else:
    lines[labelGroup] = [0,0]
  trainNum+=1


print(str(testNum) + "  " + str(trainNum))
testFile.close();
trainFile.close();

print(lines)

sortedList = sorted(lines.keys())

testList = [];
trainList = [];

for key in sortedList:
  testList.append(math.log(lines[key][0]+1));
  trainList.append(math.log(lines[key][1]+1));
print(sortedList)
print(testList)
print(trainList)


N = len(sortedList)-1
menMeans   = trainList[0:-1]
womenMeans = testList[0:-1]

ind = np.arange(N)    # the x locations for the groups
width = 0.35       # the width of the bars: can also be len(x) sequence
b, g, r, p = sns.color_palette("muted", 4)
p1 = plt.bar(ind, menMeans,   width, color='b',alpha=0.5)
p2 = plt.bar(ind, womenMeans, width, color='r',alpha=0.5,
             bottom=menMeans)

plt.ylabel('# of Sample In Logarithmic Space')
plt.xlabel('Score Groups')
plt.title('Data Set Spliting')
plt.xticks(ind+width/2., ('0~3', '4~7', '8~11', '12~15', '16~19','20~23','24~27','28~31','32~35','36~39','40~43','44~47','48~51','52~55','56~60') )
#plt.yticks(np.arange(0,2250,250))
plt.legend( (p1[0], p2[0]), ('TrainSet', 'TestSet') )

plt.show()
plt.savefig('demo.png', transparent=True)