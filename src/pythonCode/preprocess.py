#Name: Di Yang
#Email: diy@usc.edu
if __name__ == "__main__":
	import os
	import sys
	import nltk
	import re

	srcDir = sys.argv[1]
	fileB = sys.argv[2]
	fileList = os.listdir(srcDir)
	count = {"it's":0, "its":0, "you're":0, "your":0, "they're":0, "their":0, "loose":0, "lose":0, "to":0, "too":0}
	with open(fileB, "w") as foutB:
		for f in fileList:
			filePath = srcDir + '/' + f
			with open(filePath, "r", errors = 'ignore') as fin:
				text = fin.read()
				sentenceSplit = nltk.tokenize.sent_tokenize(text)
				#print(sentenceSplit)
				for s in sentenceSplit[90:]:
					s = s.replace('\n', ' ')
					shouldPrint = False
					if re.search(r"\bit's\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["it's"] += 1
					if re.search(r"\bits\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["its"] += 1
					if re.search(r"\byou're\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["you're"] += 1
					if re.search(r"\byour\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["your"] += 1
					if re.search(r"\bthey're\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["they're"] += 1
					if re.search(r"\btheir\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["their"] += 1
					if re.search(r"\bloose\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["loose"] += 1
					if re.search(r"\blose\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["lose"] += 1
					if re.search(r"\bto\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["to"] += 1
					if re.search(r"\btoo\b",s,flags=re.IGNORECASE):
						shouldPrint = True
						count["too"] += 1
					if shouldPrint == True:
						foutB.write(s + '\n')
				print(count)



			