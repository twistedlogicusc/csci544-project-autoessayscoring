

# usage: python3 length_benchmark2.py trainAll testAll testResult predictResult

import re
import sys
from sklearn.ensemble import RandomForestRegressor
import nltk
from nltk.tokenize import sent_tokenize
import score
import kenlm
import statistics
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import LogisticRegression


def removeStopWords(essay):
    tempEssay = essay.rstrip("\n");
    splittedTempEssay = tempEssay.split(" ");
    global stopWordsDict;
    newEssay = "";
    for index in range(0,len(splittedTempEssay)):
        originWord = splittedTempEssay[index];
        tempWord = originWord.lower();
        searchObj = re.search( r'([@]\w{1,})|(\w{1,})', tempWord);
        if searchObj is None:
            newEssay += originWord + " ";
        else:
            tempWord = searchObj.group(0);
            if tempWord in stopWordsDict:
                continue;
            else:
                newEssay += originWord + " ";
    newEssay = newEssay[0:len(newEssay)-1];
    return newEssay;

def add_essay_training(data, essay_set, essay, score):
    if essay_set not in data:
        data[essay_set] = {"essay":[],"score":[]}
    data[essay_set]["essay"].append(essay)
    data[essay_set]["score"].append(score)

def add_essay_test(data, essay_set, essay, prediction_id,target_score):
    #Actually prediction_id is essay_id
    if essay_set not in data:
        data[essay_set] = {"essay":[], "prediction_id":[],"targetScore":[]};
    data[essay_set]["essay"].append(essay)
    data[essay_set]["prediction_id"].append(prediction_id);
    data[essay_set]["targetScore"].append(target_score);


def read_training_data(training_file):
    f = open(training_file,'r',encoding='cp1252',errors='ignore')
    f.readline() #skip the header

    training_data = {}
    for row in f:
        row = row.rstrip('\n').split("\t")
        essay_set = row[1]
        essay = row[2]
        #domain1_score = int(row[6]);
        #set2 split into set 21 and set 22
        domain_score = int(row[3]);
        add_essay_training(training_data, essay_set, essay, domain_score)
    f.close();
    return training_data
def read_training_feature(trainingFeaturePath):
    f = open(trainingFeaturePath,'r',encoding='cp1252',errors='ignore')
    f.readline() #skip the header
    trainingFeature = {}
    for row in f:
        row = row.rstrip('\n').split(",")
        if row[1] not in trainingFeature:
            trainingFeature[row[1]] = []
        trainingFeature[row[1]].append(row[3:36])
    f.close()
    return trainingFeature
def read_test_data(test_file):
    f = open(test_file,'r',encoding='cp1252',errors='ignore');
    test_data = {}
    for row in f:
        row = row.rstrip('\n').split("\t")
        essay_set = row[1];
        essay = row[2];
        essay_id = row[0];
        targetScore = row[3];

        add_essay_test(test_data, essay_set, essay, essay_id,targetScore);
    f.close();
    #fpWrite.close();
    return test_data
def read_test_feature(testFeaturePath):
    f = open(testFeaturePath,'r',encoding='cp1252',errors='ignore')
    f.readline() #skip the header
    testFeature = {}
    for row in f:
        row = row.rstrip('\n').split(",")
        if row[1] not in testFeature:
            testFeature[row[1]] = []
        testFeature[row[1]].append(row[3:36])
    f.close()
    return testFeature
def get_character_count(essay):
    return len(essay)

def get_word_count(essay):
    return len(re.findall(r"\s", essay))+1

def get_sentence_count(essay):
    sentences = sent_tokenize(essay)
    sentenceCount = len(sentences)
    return sentenceCount

def get_distinct_word_number(essay):
    wordArr = essay.split()
    wordSet = set(wordArr)
    return len(wordSet)

def get_sentence_length(essay):
    return get_word_count(essay)/get_sentence_count(essay)

def get_3_gram_sum(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    result = 0
    sentence = ''
    if len(words) < 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        result = kenlmModel.score(sentence)
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            result += kenlmModel.score(sentence)
    return result

def get_3_gram_sqr_dev(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    result = 0
    resultList = []
    sentence = ''
    if len(words) <= 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        result = kenlmModel.score(sentence)
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            resultList.append(kenlmModel.score(sentence))
        result = statistics.stdev(resultList)
    return result

def get_3_gram_max(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    resultList = []
    sentence = ''
    maxScore = -sys.maxsize
    if len(words) <= 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        maxScore = kenlmModel.score(sentence)
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            resultList.append(kenlmModel.score(sentence))
        for m in resultList:
            if m > maxScore:
                maxScore = m
    return maxScore

def get_3_gram_range(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    resultList = []
    sentence = ''
    minScore = sys.maxsize
    maxScore = -sys.maxsize
    if len(words) <= 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        return 0
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            resultList.append(kenlmModel.score(sentence))
        for m in resultList:
            if m < minScore:
                minScore = m
        for m in resultList:
            if m > maxScore:
                maxScore = m
        return maxScore - minScore

def get_3_gram_min(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    resultList = []
    sentence = ''
    minScore = sys.maxsize
    if len(words) <= 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        minScore = kenlmModel.score(sentence)
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            resultList.append(kenlmModel.score(sentence))
        for m in resultList:
            if m < minScore:
                minScore = m
    return minScore

def get_3_gram_1_3(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    resultList = []
    sentence = ''
    if len(words) <= 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        return kenlmModel.score(sentence)
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            resultList.append(kenlmModel.score(sentence))
        resultList.sort()
        return resultList[len(resultList)//3]

def get_3_gram_median(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    resultList = []
    sentence = ''
    if len(words) <= 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        return kenlmModel.score(sentence)
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            resultList.append(kenlmModel.score(sentence))
        resultList.sort()
        return resultList[len(resultList)//2]

def get_3_gram_2_3(essay):
    #essay = removeStopWords(essay);
    global kenlmModel
    words = essay.split()
    resultList = []
    sentence = ''
    if len(words) <= 3:
        for i in range(0, len(words)):
            sentence += words[i]
            sentence = sentence.lower()
        return kenlmModel.score(sentence)
    else:
        for i in range(1, len(words)-1):
            sentence = words[i-1] + " " + words[i] + " " + words[i+1]
            sentence = sentence.lower()
            resultList.append(kenlmModel.score(sentence))
        resultList.sort()
        return resultList[len(resultList)*2//3]

def extract_features(essays, feature_functions):
    #return [[f(es) for f in feature_functions] for es in essays];
    featureVectors = [];
    for eachEssay in essays:
        eachEssayFeature = [];
        for func in feature_functions:
            eachEssayFeature.append(func(eachEssay));
        featureVectors.append(eachEssayFeature);
        del eachEssayFeature;
    return featureVectors;



def benchmark(i):
    global human_graded_file
    global auto_graded_file
    print("Reading Training Data");
    trainingDataPath = sys.argv[1];
    trainingFeaturePath = sys.argv[2]
    #training = read_training_data("../Data/training_set_rel3.tsv");
    training = read_training_data(trainingDataPath);
    training_feature_from_file = read_training_feature(trainingFeaturePath)
    #print("Reading Validation Data");
    print("Reading Test Data");
    testDataPath = sys.argv[3];
    testFeaturePath = sys.argv[4]
    test = read_test_data(testDataPath);
    test_feature_from_file = read_test_feature(testFeaturePath)
    feature_functions = [get_distinct_word_number, get_word_count, get_character_count, get_sentence_count, get_3_gram_sum, get_3_gram_sqr_dev, get_3_gram_min, get_3_gram_1_3, get_3_gram_median, get_3_gram_2_3, get_3_gram_max, get_3_gram_range];
    #feature_functions = [get_3_gram_sum, get_3_gram_sqr_dev, get_3_gram_min, get_3_gram_1_3, get_3_gram_median, get_3_gram_2_3, get_3_gram_max, get_3_gram_range];
    essay_sets = sorted(training.keys());
    #predictions = {}
    human_graded_file = sys.argv[5]
    testDataOutPath = human_graded_file;
    fpWrite2 = open(sys.argv[5], "w");
    startWrite2 = False;

    auto_graded_file = sys.argv[6]
    predictDataOutPath = auto_graded_file;
    fpWrite = open(predictDataOutPath, "w")
    startWrite = False;

    # train_features = sys.argv[5]
    # test_features = sys.argv[6]
    # train_features_file = open(train_features, 'w');
    # test_features_file = open(test_features, 'w');
    # train_features_file.write('get_3_gram_sum, get_3_gram_sqr_dev, get_3_gram_min, get_3_gram_1_3, get_3_gram_median, get_3_gram_2_3, get_3_gram_max, get_3_gram_range\n')
    # test_features_file.write('get_3_gram_sum, get_3_gram_sqr_dev, get_3_gram_min, get_3_gram_1_3, get_3_gram_median, get_3_gram_2_3, get_3_gram_max, get_3_gram_range\n')
    
    for es_set in essay_sets:
        #eachTestPrediction = [];
        print("Making Predictions for Essay Set %s" % es_set);
        # features = extract_features(training[es_set]["essay"],feature_functions);
        features = training_feature_from_file[es_set]
        # ############################
        # for ft in features:
        #     train_features_file.write(str(ft[0]) + ',' + str(ft[1]) + ',' + str(ft[2]) + ',' + str(ft[3]) + ',' + str(ft[4]) + ',' + str(ft[5]) + ',' + str(ft[6]) + ',' + str(ft[7]) + '\n')
        # ############################
        
        # rf = LogisticRegression(C=1e5);
        rf = RandomForestRegressor(n_estimators = 100, max_depth = i);
        # rf = GradientBoostingRegressor(n_estimators=100, learning_rate=0.1, max_depth=i, random_state=0, loss='ls')
        rf.fit(features,training[es_set]["score"]);
        # features = extract_features(test[es_set]["essay"], feature_functions);
        features = test_feature_from_file[es_set]
        # ############################
        # for ft in features:
        #     test_features_file.write(str(ft[0]) + ',' + str(ft[1]) + ',' + str(ft[2]) + ',' + str(ft[3]) + ',' + str(ft[4]) + ',' + str(ft[5]) + ',' + str(ft[6]) + ',' + str(ft[7]) + '\n')
        # ############################
        predicted_scores = rf.predict(features);
        
        for pred_id, pred_score, target_score in zip(test[es_set]["prediction_id"], predicted_scores, test[es_set]["targetScore"]):
            #predictions[pred_id] = round(pred_score);
            #print(str(pred_id) + "\t" + es_set + "\t" + str(round(pred_score)));
            if startWrite==False:
                startWrite = True;
                #fpWrite.write(str(pred_id) + "\t"  + str(round(pred_score)));
                fpWrite2.write(es_set + ","  + target_score);
                fpWrite.write(es_set + ","  + str(int(round(pred_score))));
            else:
                #fpWrite.write("\n" + str(pred_id) + "\t"  + str(int(round(pred_score))));
                fpWrite2.write("\n" + es_set + ","  + target_score);
                fpWrite.write("\n" + es_set + ","  + str(int(round(pred_score))));
        
    fpWrite.close();
    fpWrite2.close();
    # train_features_file.close();
    # test_features_file.close();
    """
    output_file = "../Submissions/length_benchmark.csv"
    print("Writing submission to %s" % output_file)
    f = open(output_file, "w")
    f.write("prediction_id,predicted_score\n")
    for key in sorted(predictions.keys()):
        f.write("%d,%d\n" % (key,predictions[key]))
    f.close()
    """


def evaluation(human_graded_file, auto_graded_file):
    humanScore = human_graded_file#sys.argv[3]
    autoScore = auto_graded_file#sys.argv[4]
    with open(humanScore, "r") as finHuman:
        with open(autoScore, "r") as finAuto:
            vectorHuman = []
            vectorAuto = []
            for i in range(0,8):
                temp1 = []
                temp2 = []
                vectorHuman.append(temp1)
                vectorAuto.append(temp2)
            for line in finHuman:
                line = line.split(',')
                tag = line[0]
                result = line[1].replace('\n', '')
                if tag == '1':
                    vectorHuman[0].append(int(result))
                if tag == '21' or tag == '22':
                    vectorHuman[1].append(int(result))
                if tag == '3':
                    vectorHuman[2].append(int(result))
                if tag == '4':
                    vectorHuman[3].append(int(result))
                if tag == '5':
                    vectorHuman[4].append(int(result))
                if tag == '6':
                    vectorHuman[5].append(int(result))
                if tag == '7':
                    vectorHuman[6].append(int(result))
                if tag == '8':
                    vectorHuman[7].append(int(result))

            for line in finAuto:
                line = line.split(',')
                tag = line[0]
                result = line[1].replace('\n', '')
                if tag == '1':
                    vectorAuto[0].append(int(result))
                if tag == '21' or tag == '22':
                    vectorAuto[1].append(int(result))
                if tag == '3':
                    vectorAuto[2].append(int(result))
                if tag == '4':
                    vectorAuto[3].append(int(result))
                if tag == '5':
                    vectorAuto[4].append(int(result))
                if tag == '6':
                    vectorAuto[5].append(int(result))
                if tag == '7':
                    vectorAuto[6].append(int(result))
                if tag == '8':
                    vectorAuto[7].append(int(result))
            qwk = []
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[0],vectorAuto[0],2,12))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[1],vectorAuto[1],1,6))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[2],vectorAuto[2],0,3))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[3],vectorAuto[3],0,3))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[4],vectorAuto[4],0,4))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[5],vectorAuto[5],0,4))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[6],vectorAuto[6],0,30))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[7],vectorAuto[7],0,60))
            for i in range(0,8):
                print("qwk_{0}: ".format(i+1) + str(qwk[i]))
            print("mean_quadratic_weighted_kappa: " + str(score.mean_quadratic_weighted_kappa(qwk)))
if __name__=="__main__":

    human_graded_file = 0
    auto_graded_file = 0
    kenlmModel = kenlm.LanguageModel("textSource.arpa")
    print(score.mean_quadratic_weighted_kappa([0.8747677009080869,0.6727837946249497,0.6801003479809016,0.76566,0.8220458109546522,0.8160000000000001,0.7412141,0.6337094374454704]))
    # print(score.mean_quadratic_weighted_kappa([0.809,0.658,0.538,0.687,0.678,0.780,0.675,0.721,0.698]))
    for i in range(1, 10):
        benchmark(i)
        evaluation(human_graded_file, auto_graded_file)
