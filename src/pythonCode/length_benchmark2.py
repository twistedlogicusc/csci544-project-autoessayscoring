

# usage: python3 length_benchmark2.py trainAll testAll testResult predictResult

import re
import sys
from sklearn.ensemble import RandomForestRegressor

def add_essay_training(data, essay_set, essay, score):
    if essay_set not in data:
        data[essay_set] = {"essay":[],"score":[]}
    data[essay_set]["essay"].append(essay)
    data[essay_set]["score"].append(score)

def add_essay_test(data, essay_set, essay, prediction_id,target_score):
    #Actually prediction_id is essay_id
    if essay_set not in data:
        data[essay_set] = {"essay":[], "prediction_id":[],"targetScore":[]};
    data[essay_set]["essay"].append(essay)
    data[essay_set]["prediction_id"].append(prediction_id);
    data[essay_set]["targetScore"].append(target_score);


def read_training_data(training_file):
    f = open(training_file,'r',encoding='cp1252',errors='ignore')
    f.readline() #skip the header

    training_data = {}
    for row in f:
        row = row.rstrip('\n').split("\t")
        essay_set = row[1]
        essay = row[2]
        #domain1_score = int(row[6]);
        #set2 split into set 21 and set 22
        domain_score = int(row[3]);
        add_essay_training(training_data, essay_set, essay, domain_score)

        """
        domain1_score = int(row[3])
        if essay_set == "2":
            essay_set = "2_1"
        add_essay_training(training_data, essay_set, essay, domain1_score)
        
        if essay_set == "2_1":
            essay_set = "2_2"
            domain2_score = int(row[9])
            add_essay_training(training_data, essay_set, essay, domain2_score)
        """
    f.close();
    return training_data

def read_test_data(test_file):
    f = open(test_file,'r',encoding='cp1252',errors='ignore');

    #fpWrite = open(sys.argv[3], "w");
    #startWrite = False;
    
    #f.readline()
    test_data = {}
    for row in f:
        row = row.rstrip('\n').split("\t")
        essay_set = row[1];
        essay = row[2];

        """
        domain1_predictionid = int(row[3])
        if essay_set == "2": 
            domain2_predictionid = int(row[4])
            add_essay_test(test_data, "2_1", essay, domain1_predictionid)
            add_essay_test(test_data, "2_2", essay, domain2_predictionid)
        else:
            add_essay_test(test_data, essay_set, essay, domain1_predictionid)
        """
        essay_id = row[0];
        targetScore = row[3];

        add_essay_test(test_data, essay_set, essay, essay_id,targetScore);
        """
        if startWrite==False:
            startWrite = True;
            fpWrite.write(essay_set + "," + row[3]);
        else:
            fpWrite.write("\n" + essay_set + "," + row[3]);
        """

    f.close();
    #fpWrite.close();
    return test_data


def get_character_count(essay):
    return len(essay)

def get_word_count(essay):
    return len(re.findall(r"\s", essay))+1

def extract_features(essays, feature_functions):
    #return [[f(es) for f in feature_functions] for es in essays];
    featureVectors = [];
    for eachEssay in essays:
        eachEssayFeature = [];
        for func in feature_functions:
            eachEssayFeature.append(func(eachEssay));
        featureVectors.append(eachEssayFeature);
        del eachEssayFeature;
    return featureVectors;



def main():
    
    print("Reading Training Data");
    trainingDataPath = sys.argv[1];
    #training = read_training_data("../Data/training_set_rel3.tsv");
    training = read_training_data(trainingDataPath);

    #print("Reading Validation Data");
    print("Reading Test Data");
    testDataPath = sys.argv[2];
    test = read_test_data(testDataPath);
    feature_functions = [get_character_count, get_word_count];
    essay_sets = sorted(training.keys());
    #predictions = {}

    testDataOutPath = sys.argv[3];
    fpWrite2 = open(sys.argv[3], "w");
    startWrite2 = False;


    predictDataOutPath = sys.argv[4];
    fpWrite = open(predictDataOutPath, "w")
    startWrite = False;
    for es_set in essay_sets:
        #eachTestPrediction = [];
        print("Making Predictions for Essay Set %s" % es_set);
        features = extract_features(training[es_set]["essay"],feature_functions);
        rf = RandomForestRegressor(n_estimators = 100);
        rf.fit(features,training[es_set]["score"]);
        features = extract_features(test[es_set]["essay"], feature_functions);
        predicted_scores = rf.predict(features);
        
        for pred_id, pred_score, target_score in zip(test[es_set]["prediction_id"], predicted_scores, test[es_set]["targetScore"]):
            #predictions[pred_id] = round(pred_score);
            #print(str(pred_id) + "\t" + es_set + "\t" + str(round(pred_score)));
            if startWrite==False:
                startWrite = True;
                #fpWrite.write(str(pred_id) + "\t"  + str(round(pred_score)));
                fpWrite2.write(es_set + ","  + target_score);
                fpWrite.write(es_set + ","  + str(int(round(pred_score))));
            else:
                #fpWrite.write("\n" + str(pred_id) + "\t"  + str(int(round(pred_score))));
                fpWrite2.write("\n" + es_set + ","  + target_score);
                fpWrite.write("\n" + es_set + ","  + str(int(round(pred_score))));
        
    fpWrite.close();
    fpWrite2.close();
    """
    output_file = "../Submissions/length_benchmark.csv"
    print("Writing submission to %s" % output_file)
    f = open(output_file, "w")
    f.write("prediction_id,predicted_score\n")
    for key in sorted(predictions.keys()):
        f.write("%d,%d\n" % (key,predictions[key]))
    f.close()
    """
if __name__=="__main__":
    main()
