#Name: Di Yang
#Email: diy@usc.edu
if __name__ == "__main__":
	import os
	import sys
	import re
	fileA = sys.argv[1]
	fileB = sys.argv[2]
	lineNum = 0

	itsVS = 0
	yourVs = 0
	theirVs = 0
	looseVs = 0
	toVs = 0

	itsVSTrue = 0
	yourVsTrue = 0
	theirVsTrue = 0
	looseVsTrue = 0
	toVsTrue = 0
	with open(fileA, "r", errors="ignore") as finA, open(fileB, 'r') as finB:
		for x, y in zip(finA, finB):
			x = x.split()
			y = y.split()
			for i, s in enumerate(x):
				if re.search(r"\bit's\b",s,flags=re.IGNORECASE):
					itsVS += 1
					if x[i] == y[i]:
						itsVSTrue += 1
				if re.search(r"\bits\b",s,flags=re.IGNORECASE):
					itsVS += 1
					if x[i] == y[i]:
						itsVSTrue += 1
				if re.search(r"\byou're\b",s,flags=re.IGNORECASE):
					yourVs += 1
					if x[i] == y[i]:
						yourVsTrue += 1
				if re.search(r"\byour\b",s,flags=re.IGNORECASE):
					yourVs += 1
					if x[i] == y[i]:
						yourVsTrue += 1
				if re.search(r"\bthey're\b",s,flags=re.IGNORECASE):
					theirVs += 1
					if x[i] == y[i]:
						theirVsTrue += 1
				if re.search(r"\btheir\b",s,flags=re.IGNORECASE):
					theirVs += 1
					if x[i] == y[i]:
						theirVsTrue += 1
				if re.search(r"\bloose\b",s,flags=re.IGNORECASE):
					looseVs += 1
					if x[i] == y[i]:
						looseVsTrue += 1
				if re.search(r"\blose\b",s,flags=re.IGNORECASE):
					looseVs += 1
					if x[i] == y[i]:
						looseVsTrue += 1
				if re.search(r"\bto\b",s,flags=re.IGNORECASE):
					toVs += 1
					if x[i] == y[i]:
						toVsTrue += 1
				if re.search(r"\btoo\b",s,flags=re.IGNORECASE):
					toVs += 1
					if x[i] == y[i]:
						toVsTrue += 1
		print("itsVS: " + str(itsVS) + " itsVSTrue: " + str(itsVSTrue))
		print("yourVs: " + str(yourVs) + " yourVsTrue: " + str(yourVsTrue))
		print("theirVs: " + str(theirVs) + " theirVsTrue: " + str(theirVsTrue))
		print("looseVs: " + str(looseVs) + " looseVsTrue: " + str(looseVsTrue))
		print("toVs: " + str(toVs) + " toVsTrue: " + str(toVsTrue))