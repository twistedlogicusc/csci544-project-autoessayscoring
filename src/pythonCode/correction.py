#Name: Di Yang
#Email: diy@usc.edu
if __name__ == "__main__":
	import os
	import sys
	import re
	import kenlm

	arpaModelFile = sys.argv[1]
	errFile = sys.argv[2]
	outputFile = sys.argv[3]
	kenlmModel = kenlm.LanguageModel(arpaModelFile)

	def calculateScore(words, i, origStr, compStr):
		lowerCaseFlag = False
		thisStr = words[i]
		replaceString = ''
		if thisStr.islower():
			lowerCaseFlag = True
		thisStr = thisStr.lower()
		if lowerCaseFlag == False: #upperCase
			replaceString = thisStr.replace(origStr, compStr.capitalize())
		else: #lowerCase
			replaceString = thisStr.replace(origStr, compStr)
		low = i-2
		high = i+2
		while(low<0 or high>len(words)-1):
			if low < 0:
				low += 1
			if high > len(words)-1:
				high -= 1
		origScore = 0
		origSentense = ''
		compScore = 0
		compSentense = ''
		if high-low < 2:
			for m in range(low, high+1):
				origSentense += words[m] + ' '
			origSentense = origSentense.lower()
			origScore = kenlmModel.score(origSentense)
			for m in range(low, high+1):
				if m == i:
					compSentense += replaceString + ' '
				else:
					compSentense += words[m] + ' '
			compSentense = compSentense.lower()
			compScore = kenlmModel.score(compSentense)
			#print("orig: " + origSentense)
			#print("comp: " + compSentense)
			if compScore > origScore:
				words[i] = replaceString
		else:
			for m in range(low, high-2+1):
				origSentense += words[m] + ' ' + words[m+1] + ' ' + words[m+2]
				origSentense = origSentense.lower()
				origScore += kenlmModel.score(origSentense)
				# print("origSentense: " + origSentense)
				# print("origScore: " + str(kenlmModel.score(origSentense)))
				# print("origScoreTotal: " + str(origScore))
				origSentense = ''				
			for m in range(low, high-2+1):
				if m == i:
					compSentense += replaceString + ' ' + words[m+1] + ' ' + words[m+2]
				elif m+1 == i:
					compSentense += words[m] + ' ' + replaceString + ' ' + words[m+2]
				elif m+2 == i:
					compSentense += words[m] + ' ' + words[m+1] + ' ' + replaceString
				else:
					print("shit!")
				compSentense = compSentense.lower()
				compScore += kenlmModel.score(compSentense)
				# print("compSentense: " + compSentense)
				# print("compScore: " + str(kenlmModel.score(compSentense)))
				# print("compScoreTotal: " + str(compScore))
				compSentense = ''
			if compScore > origScore:
				words[i] = replaceString
				#print("replace! "+replaceString)

	with open(outputFile, "w") as fout:
		with open(errFile, "r", errors = 'ignore') as fin:
			for line in fin:
				words = line.split()
				for i in range(0, len(words)):
					s = words[i]
					if re.search(r"\bit's\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "it's", "its")
					if re.search(r"\bits\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "its", "it's")
					if re.search(r"\byou're\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "you're", "your")
					if re.search(r"\byour\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "your", "you're")
					if re.search(r"\bthey're\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "they're", "their")
					if re.search(r"\btheir\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "their", "they're")
					if re.search(r"\bloose\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "loose", "lose")
					if re.search(r"\blose\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "lose", "loose")
					if re.search(r"\bto\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "to", "too")
					if re.search(r"\btoo\b",s,flags=re.IGNORECASE):
						calculateScore(words, i, "too", "to")
				# print(words)
				for w in words:
					fout.write(w + ' ')
				fout.write('\n')

