#Name: Di Yang
#Email: diy@usc.edu
if __name__ == "__main__":
	import os
	import sys
	import re
	import nltk
	import kenlm

	arpaModelFile = sys.argv[1]
	textFile = sys.argv[2]
	outputFile = sys.argv[3]
	kenlmModel = kenlm.LanguageModel(arpaModelFile)

	with open(outputFile, "w") as foutB:
		with open(textFile, "r", errors = 'ignore') as fin:
			for line in fin:
				words = line.split()
				for i in range(1, len(words)-1):
					sentence = words[i-1] + " " + words[i] + " " + words[i+1]
					score = kenlmModel.score(sentence)
					foutB.write(str(score) + "\t")
				foutB.write("\n")


