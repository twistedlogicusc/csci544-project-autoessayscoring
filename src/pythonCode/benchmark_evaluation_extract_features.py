

# usage: python3 length_benchmark2.py trainAll testAll testResult predictResult

import re
import sys
from sklearn.ensemble import RandomForestRegressor
import nltk
from nltk.tokenize import sent_tokenize
import score
import itertools
from itertools import product

def add_essay_training(data, essay_set, essay, score):
    if essay_set not in data:
        data[essay_set] = {"essay":[],"score":[]}
    data[essay_set]["essay"].append(essay)
    data[essay_set]["score"].append(score)

def add_essay_test(data, essay_set, essay, prediction_id,target_score):
    #Actually prediction_id is essay_id
    if essay_set not in data:
        data[essay_set] = {"essay":[], "prediction_id":[],"targetScore":[]};
    data[essay_set]["essay"].append(essay)
    data[essay_set]["prediction_id"].append(prediction_id);
    data[essay_set]["targetScore"].append(target_score);


def read_training_data(training_file):
    f = open(training_file,'r',encoding='cp1252',errors='ignore')
    f.readline() #skip the header

    training_data = {}
    for row in f:
        row = row.rstrip('\n').split("\t")
        essay_set = row[1]
        essay = row[2]
        #domain1_score = int(row[6]);
        #set2 split into set 21 and set 22
        domain_score = int(row[3]);
        add_essay_training(training_data, essay_set, essay, domain_score)

        """
        domain1_score = int(row[3])
        if essay_set == "2":
            essay_set = "2_1"
        add_essay_training(training_data, essay_set, essay, domain1_score)
        
        if essay_set == "2_1":
            essay_set = "2_2"
            domain2_score = int(row[9])
            add_essay_training(training_data, essay_set, essay, domain2_score)
        """
    f.close();
    return training_data

def read_test_data(test_file):
    f = open(test_file,'r',encoding='cp1252',errors='ignore');

    #fpWrite = open(sys.argv[3], "w");
    #startWrite = False;
    
    #f.readline()
    test_data = {}
    for row in f:
        row = row.rstrip('\n').split("\t")
        essay_set = row[1];
        essay = row[2];

        """
        domain1_predictionid = int(row[3])
        if essay_set == "2": 
            domain2_predictionid = int(row[4])
            add_essay_test(test_data, "2_1", essay, domain1_predictionid)
            add_essay_test(test_data, "2_2", essay, domain2_predictionid)
        else:
            add_essay_test(test_data, essay_set, essay, domain1_predictionid)
        """
        essay_id = row[0];
        targetScore = row[3];

        add_essay_test(test_data, essay_set, essay, essay_id,targetScore);
        """
        if startWrite==False:
            startWrite = True;
            fpWrite.write(essay_set + "," + row[3]);
        else:
            fpWrite.write("\n" + essay_set + "," + row[3]);
        """

    f.close();
    #fpWrite.close();
    return test_data


def get_character_count(essay):
    return len(essay)

def get_word_count(essay):
    return len(re.findall(r"\s", essay))+1

def get_sentence_count(essay):
    sentences = sent_tokenize(essay)
    sentenceCount = len(sentences)
    return sentenceCount

def get_distinct_word_number(essay):
    wordArr = essay.split()
    wordSet = set(wordArr)
    return len(wordSet)

def get_sentence_length(essay):
    return get_word_count(essay)/get_sentence_count(essay)

def extract_features(essays, feature_functions):
    #return [[f(es) for f in feature_functions] for es in essays];
    featureVectors = [];
    for eachEssay in essays:
        eachEssayFeature = [];
        for func in feature_functions:
            eachEssayFeature.append(func(eachEssay));
        featureVectors.append(eachEssayFeature);
        del eachEssayFeature;
    return featureVectors;



def benchmark(l):
    global human_graded_file
    global auto_graded_file
    #print("Reading Training Data");
    trainingDataPath = sys.argv[1];
    #training = read_training_data("../Data/training_set_rel3.tsv");
    training = read_training_data(trainingDataPath);

    #print("Reading Validation Data");
    #print("Reading Test Data");
    testDataPath = sys.argv[2];
    test = read_test_data(testDataPath);
    #feature_functions = [get_distinct_word_number, get_word_count, get_character_count, get_sentence_count];#get_character_count, get_word_count get_sentence_count
    feature_functions = l
    essay_sets = sorted(training.keys());
    #predictions = {}
    human_graded_file = sys.argv[3]
    testDataOutPath = human_graded_file;
    fpWrite2 = open(sys.argv[3], "w");
    startWrite2 = False;

    auto_graded_file = sys.argv[4]
    predictDataOutPath = auto_graded_file;
    fpWrite = open(predictDataOutPath, "w")
    startWrite = False;

    train_features = sys.argv[5]
    test_features = sys.argv[6]
    train_features_file = open(train_features, 'w');
    test_features_file = open(test_features, 'w');
#    func_list = [get_distinct_word_number, get_word_count, get_character_count, get_sentence_count];
    train_features_file.write('DWN,WC,CH_LEN,SC\n')
    test_features_file.write('DWN,WC,CH_LEN,SC\n')


    for es_set in essay_sets:
        #eachTestPrediction = [];
        #print("Making Predictions for Essay Set %s" % es_set);
        features = extract_features(training[es_set]["essay"],feature_functions);
############################
        for ft in features:
            train_features_file.write(str(ft[0]) + ',' + str(ft[1]) + ',' + str(ft[2]) + ',' + str(ft[3]) + '\n')
############################
        #print(features);
        #print('\n\n\n')
        rf = RandomForestRegressor(n_estimators = 100);
        rf.fit(features,training[es_set]["score"]);
        features = extract_features(test[es_set]["essay"], feature_functions);
############################
        for ft in features:
            test_features_file.write(str(ft[0]) + ',' + str(ft[1]) + ',' + str(ft[2]) + ',' + str(ft[3]) + '\n')
############################
        predicted_scores = rf.predict(features);
        
        for pred_id, pred_score, target_score in zip(test[es_set]["prediction_id"], predicted_scores, test[es_set]["targetScore"]):
            #predictions[pred_id] = round(pred_score);
            #print(str(pred_id) + "\t" + es_set + "\t" + str(round(pred_score)));
            if startWrite==False:
                startWrite = True;
                #fpWrite.write(str(pred_id) + "\t"  + str(round(pred_score)));
                fpWrite2.write(es_set + ","  + target_score);
                fpWrite.write(es_set + ","  + str(int(round(pred_score))));
            else:
                #fpWrite.write("\n" + str(pred_id) + "\t"  + str(int(round(pred_score))));
                fpWrite2.write("\n" + es_set + ","  + target_score);
                fpWrite.write("\n" + es_set + ","  + str(int(round(pred_score))));
        
    fpWrite.close();
    fpWrite2.close();
    train_features_file.close();
    test_features_file.close();
    """
    output_file = "../Submissions/length_benchmark.csv"
    print("Writing submission to %s" % output_file)
    f = open(output_file, "w")
    f.write("prediction_id,predicted_score\n")
    for key in sorted(predictions.keys()):
        f.write("%d,%d\n" % (key,predictions[key]))
    f.close()
    """


def evaluation(human_graded_file, auto_graded_file):
    humanScore = human_graded_file#sys.argv[3]
    autoScore = auto_graded_file#sys.argv[4]
    with open(humanScore, "r") as finHuman:
        with open(autoScore, "r") as finAuto:
            vectorHuman = []
            vectorAuto = []
            for i in range(0,8):
                temp1 = []
                temp2 = []
                vectorHuman.append(temp1)
                vectorAuto.append(temp2)
            for line in finHuman:
                line = line.split(',')
                tag = line[0]
                result = line[1].replace('\n', '')
                if tag == '1':
                    vectorHuman[0].append(int(result))
                if tag == '21' or tag == '22':
                    vectorHuman[1].append(int(result))
                if tag == '3':
                    vectorHuman[2].append(int(result))
                if tag == '4':
                    vectorHuman[3].append(int(result))
                if tag == '5':
                    vectorHuman[4].append(int(result))
                if tag == '6':
                    vectorHuman[5].append(int(result))
                if tag == '7':
                    vectorHuman[6].append(int(result))
                if tag == '8':
                    vectorHuman[7].append(int(result))

            for line in finAuto:
                line = line.split(',')
                tag = line[0]
                result = line[1].replace('\n', '')
                if tag == '1':
                    vectorAuto[0].append(int(result))
                if tag == '21' or tag == '22':
                    vectorAuto[1].append(int(result))
                if tag == '3':
                    vectorAuto[2].append(int(result))
                if tag == '4':
                    vectorAuto[3].append(int(result))
                if tag == '5':
                    vectorAuto[4].append(int(result))
                if tag == '6':
                    vectorAuto[5].append(int(result))
                if tag == '7':
                    vectorAuto[6].append(int(result))
                if tag == '8':
                    vectorAuto[7].append(int(result))
            qwk = []
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[0],vectorAuto[0],2,12))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[1],vectorAuto[1],1,6))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[2],vectorAuto[2],0,3))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[3],vectorAuto[3],0,3))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[4],vectorAuto[4],0,4))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[5],vectorAuto[5],0,4))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[6],vectorAuto[6],0,30))
            qwk.append(score.quadratic_weighted_kappa(vectorHuman[7],vectorAuto[7],0,60))
            for i in range(0,8):
                print("qwk_{0}: ".format(i+1) + str(qwk[i]))
            print("mean_quadratic_weighted_kappa: " + str(score.mean_quadratic_weighted_kappa(qwk)))
if __name__=="__main__":
    human_graded_file = 0
    auto_graded_file = 0
    #func_list = [get_distinct_word_number, get_word_count, get_character_count, get_sentence_count, get_sentence_length];
    '''for i in (1, 2, 3, 4, 5):
        combinations = itertools.combinations(func_list, i)
        for combination in combinations:
            print(combination)
            benchmark(combination)
            evaluation(human_graded_file, auto_graded_file)
            print('\n\n')'''
    func_list = [get_distinct_word_number, get_word_count, get_character_count, get_sentence_count];
    #func_list = [get_word_count];
    benchmark(func_list)
    #benchmark()
    evaluation(human_graded_file, auto_graded_file)
