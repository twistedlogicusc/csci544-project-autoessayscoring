import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
sns.set(style="white", context="talk")
rs = np.random.RandomState(7)

#1 0.80    #32 0.762  #50 0.740 #stanford 0.73 #CMU 0.70 #100 0.682 #bench 0.647 #140 0.510 #142 0.310
list1=['Champion', 'Our Team','top 50','Stanford','CMU','top 100','benchmark','top 140','Last']
list1=['1','2','3','4','5','6','7','8','9']
x = np.array(list1)

f, ax1 = plt.subplots(1, 1, figsize=(8, 6), sharex=False)


y2 = np.array([806-647,762-647,740-647,730-647,700-647,682-647,647-647,510-647,310-647])

sns.barplot(x, y2, ci=None, palette="coolwarm", hline=1, ax=ax1)

ax1.set_ylabel("Score")



sns.despine(bottom=True)
plt.setp(f.axes, yticks=[])
plt.show()