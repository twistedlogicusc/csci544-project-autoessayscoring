#Di Yang
#diy@usc.edu
#python3 preprocess_highScore.py trainAll textFile_highScore.txt

if __name__ == "__main__":
	import os
	import sys
	import nltk
	import re

	srcFile = sys.argv[1]
	fileB = sys.argv[2]
	with open(fileB, "w") as foutB:
		with open(srcFile, "r", errors = 'ignore') as fin:
			for line in fin:
				words = line.split()
				if words[1] == '1' and int(words[len(words)-1]) >= 7:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')
				if (words[1] == '21' or words[1] == '22') and int(words[len(words)-1]) >= 4:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')
				if words[1] == '3' and int(words[len(words)-1]) >= 2:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')
				if words[1] == '4' and int(words[len(words)-1]) >= 2:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')
				if words[1] == '5' and int(words[len(words)-1]) >= 3:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')
				if words[1] == '6' and int(words[len(words)-1]) >= 3:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')
				if words[1] == '7' and int(words[len(words)-1]) >= 15:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')
				if words[1] == '8' and int(words[len(words)-1]) >= 30:
					for i in range(2,len(words)-1):
						foutB.write(words[i] + ' ')
					foutB.write('\n')