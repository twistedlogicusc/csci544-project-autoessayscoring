import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sns.set(style="white", palette="muted")
f, axes = plt.subplots(1, 1, figsize=(7, 7), sharex=False)
sns.despine(left=True)

rs = np.random.RandomState(10)

#b, g, r, p = sns.color_palette("muted", 4)

d = rs.normal(size=100)
print(d)
b, g, r, p = sns.color_palette("muted", 4)

sns.distplot(d, color=p)

plt.setp(axes, yticks=[])
plt.show()
plt.savefig('demo.png', transparent=True)